//
//  ViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    @IBOutlet weak var billTextField: UITextField!
    @IBOutlet weak var twentyPercentButton: UIButton!
    @IBOutlet weak var tenPercentButton: UIButton!
    @IBOutlet weak var zeroPercentButton: UIButton!
    @IBOutlet weak var splitNumberLabel: UILabel!
    
    var currentPct = 0.1
    var pctButtons: [UIButton] = []
    var result = 0.0
    var data: [String: Double] = [
        "total": 0,
        "personCount": 0,
        "tip": 0,
    ]

    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        let price = Double(billTextField.text!) ?? 1
        let pct = currentPct + 1
        let personCount = Double(splitNumberLabel.text!)!
        result = price * pct / personCount
        data["total"] = round(result * 100) / 100
        data["personCount"] = personCount
        data["tip"] = currentPct * 100
        performSegue(withIdentifier: "goToResult", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            let destinationVC = segue.destination as! ResultViewController
            destinationVC.data = data
        }
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        splitNumberLabel.text = String(Int(sender.value))
    }

    @IBAction func tipChanged(_ sender: UIButton) {
        pctButtons.forEach({ $0.isSelected = false })
        sender.isSelected = true
        
        switch sender.currentTitle {
        case "0%" : currentPct = 0
        case "10%": currentPct = 0.1
        case "20%": currentPct = 0.2
        default   : currentPct = 1
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pctButtons = [zeroPercentButton, tenPercentButton, twentyPercentButton]
        // Do any additional setup after loading the view.
    }


}


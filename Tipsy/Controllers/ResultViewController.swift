//
//  ResultViewController.swift
//  Tipsy
//
//  Created by Константин Стольников on 2023/02/15.
//  Copyright © 2023 The App Brewery. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    var resultValue: String?
    var data: [String: Double] = [
        "total": 0,
        "personCount": 0,
        "tip": 0,
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        resultLabel.text = String(data["total"] ?? 0)
        settingsLabel.text = "Split between \(Int(data["personCount"]!)) people with \(Int(data["tip"]!))% tip."
    }
    
    @IBAction func recalculatePressed(_ sender: UIButton) {
        self.dismiss(animated: true)
    }
}
